﻿using System;
using System.IO;

namespace DepthAndWidthBypass
{
    public class WorkWithFile
    {
        public int[,] ReadFile()
        {
            string path = @"../../../TheAdjacencyMatrixOfAGraph.txt";
            var txt = File.ReadAllLines(path); //узнаем "ширину" матрицы
            int width = txt[0].Split(' ').Length; //узнаем "длину" матрицы
            int[,] matrix = new int[txt.Length, width];
            for (int i = 0; i < txt.Length; i++)
            {
                var str = txt[i].Split(' ');
                for (int j = 0; j < width; j++)
                    matrix[i, j] = Convert.ToInt32(str[j]);
            }

            return matrix;
        }
    }
}