﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DepthAndWidthBypass
{
    public static class Bypass
    {
        public static void DepthBypass(Top startTop, int countTop) //обход в глубину
        {
            Console.WriteLine("Обход в глубину будет производиться в таком порядке:");
            var stack = new Stack<Top>();
            stack.Push(startTop);
            var visited = new HashSet<Top>();
            while (visited.Count != countTop)
            {
                var top = stack.Pop();
                if (!visited.Contains(top))
                {
                    visited.Add(top);
                    Console.WriteLine(top.Name);
                }

                foreach (var edge in top.Edges.Where(
                    edge => top.Edges.Count > 1 && !visited.Contains(edge.ConnectedTop)))
                    stack.Push(edge.ConnectedTop);
            }
        }

        public static void WidthBypass(Top startTop, int countTop) //обход в ширину
        {
            Console.WriteLine("Обход в ширину будет производиться в таком порядке:");
            var queue = new Queue<Top>();
            queue.Enqueue(startTop);
            var visited = new HashSet<Top>();
            while (visited.Count != countTop)
            {
                var top = queue.Dequeue();
                if (!visited.Contains(top))
                {
                    visited.Add(top);
                    Console.WriteLine(top.Name);
                }

                foreach (var edge in top.Edges.Where(edge => top.Edges.Count > 1))
                    queue.Enqueue(edge.ConnectedTop);
            }
        }
    }
}