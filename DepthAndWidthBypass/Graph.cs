﻿using System.Collections.Generic;
using System.Linq;

namespace DepthAndWidthBypass
{
    public class Graph
    {
        public List<Top> ListTop { get; }

        public Graph()
        {
            ListTop = new List<Top>();
        }

        private void AddTop(char name) //добавляем вершину
        {
            ListTop.Add(new Top(name));
        }

        private void AddEdge(int value, char startName, char endName) //добавляем ребро
        {
            var startTop = SeacrhTop(startName);
            var endTop = SeacrhTop(endName);
            if (startTop != null && endTop != null)
            {
                startTop.AddEdge(new Edge(value, endTop));
                if (startTop != endTop)
                    endTop.AddEdge(new Edge(value, startTop));
            }
        }

        private Top SeacrhTop(char name) //ищем вершину по имени
        {
            return ListTop.FirstOrDefault(top => top.Name == name);
        }

        public Graph BuildGraph()
        {
            var matrix = new WorkWithFile().ReadFile();
            var graph = new Graph();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = i; j < matrix.GetLength(1); j++)
                {
                    var value = matrix[i, j];
                    if (value > 0)
                    {
                        if (graph.SeacrhTop((char) (i + 65)) == null)
                            graph.AddTop((char) (i + 65));
                        if (graph.SeacrhTop((char) (j + 65)) == null)
                            graph.AddTop((char) (j + 65));
                        graph.AddEdge(value, (char) (i + 65), (char) (j + 65));
                    }
                }
            }

            return graph;
        }
    }
}