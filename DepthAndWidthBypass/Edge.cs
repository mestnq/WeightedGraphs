﻿namespace DepthAndWidthBypass
{
    public class Edge //ребро
    {
        public int Value { get; set; }
        public Top ConnectedTop { get; }

        public Edge(int value, Top connectedTop)
        {
            Value = value;
            ConnectedTop = connectedTop;
        }
    }
}