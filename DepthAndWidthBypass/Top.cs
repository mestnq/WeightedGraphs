﻿using System.Collections.Generic;

namespace DepthAndWidthBypass
{
    public class Top //вершина
    {
        public char Name { get; }
        public List<Edge> Edges { get; }
        
        public Top(char name)
        {
            Name = name;
            Edges = new List<Edge>();
        }

        public void AddEdge(Edge edge) //добавляем ребро
        {
            Edges.Add(edge);
        }
    }
}