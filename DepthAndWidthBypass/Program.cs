﻿namespace DepthAndWidthBypass
{
    class Program
    {
        static void Main(string[] args)
        {
            var graph = new Graph().BuildGraph();
            Bypass.WidthBypass(graph.ListTop[0], graph.ListTop.Count);
            Bypass.DepthBypass(graph.ListTop[0], graph.ListTop.Count);
        }
    }
}